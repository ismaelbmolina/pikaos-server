var nodemailer = require('nodemailer');
var authentication = require("./authentication");

var transporter = nodemailer.createTransport({
    service:'Gmail',
    secure: true,
    auth: {
        user: 'pikaosapp@gmail.com',
        pass: 'Pikaos1234*'
    }
});

//En desuso por digital ocean esto esta usandose en otro server de apoyo que lo llamo con una request desde el registro
module.exports.sendRegisterMail = (email,user)=>{

    var emailEncripted = authentication.createTokenRegister(email);

    var mailOptions = {
        from: 'Pikaos',
        to: email,
        subject: 'Validación de tu correo en pikaos',
        html: '<h1>¡'+user+' te damos la bienvenida a pikaos!</h1><p>Pulsa en el siguiente enlace para <strong>validar</strong> tu cuenta: http://157.230.114.223:8090/register/validate/'+emailEncripted+'</p>'
    };

    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}

module.exports.videogameProposal = (videogame,information,individual,team,customGames)=>{
    
    var mailOptions = {
        from: 'videogame Proposal',
        to: 'pikaosapp@gmail.com',
        subject: "Propuesta videojuego: " + videogame,
        html: "<p><strong>individual:</strong> "+individual+"</p>"+
        "<p><strong>team:</strong> "+team+"</p>"+
        "<p><strong>customGames:</strong> "+customGames+"</p>"+
        "<p><strong>Information:</strong> "+information+"</p>"
    };

    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}

module.exports.reportAdmin = (user, competition, description)=>{

    var mailOptions = {
        from: 'Player Report',
        to: 'pikaosapp@gmail.com',
        subject: "User: " + user + "\tCompetition: "+competition,
        text: "Description: "+ description
    };


    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}


    