var express = require('express');
var router = express.Router();
var db = require('../db');
var authentication = require("../authentication");
var errors = require("../errors");
var request = require("request");
var crypto = require('crypto');

//Respuestas
//200: token y nombre del usuario
//401: Error en las credenciales
//400: email sin verificar
router.post('/', function (req, res) {

    try{
        var user = req.body.user;
        var password = req.body.password;

        if(user==null || password==null)
            res.status(403).send({message: "Se necesita un usuario y una contraseña"});
        else{

            db.query("SELECT id,email_verified,name,avatar,status from user where (name = ? OR email=?)  AND password=?",[user,user,password],(error,result)=>{
                if(result.length!=1)
                    res.status(401).send("-1");
                else{
                    //Comprobar que el usuario ha validado la cuenta 
                    if(result[0].email_verified!=1)
                        res.status(400).send("-2");
                    else{
                        //Generar token
                        var token =  authentication.createToken(result[0].id);
                        var name = result[0].name;

                        var response = {
                            token: token,
                            user: name,
                            avatar: result[0].avatar,
                            status: result[0].status
                        };

                        console.log("Login user: " + result[0].id)

                        res.status(200).send(response);
                    }
                }
            });
        }
    }catch(e){
        errors.registerError(e);
        res.status(500).send({message: "Error inesperado"});
    }
});

//200: Todo OK
//400: Correo no existe
router.post('/sendverifyemail',(req,res)=>{
    try{
        var credential = req.body.credential;

        db.query("SELECT name,email FROM user WHERE email = ? OR name = ?",[credential,credential],(err,resS)=>{
            if(resS.length==0)
                req.status(400).send();
            else{
                //Mandar al otro servidor una petición para que mande el correo ya que digital ocean me da millones de problemas
                var options = {
                    url: 'http://89.39.159.65:8090/',
                    method: 'POST',
                    form: {'user': resS[0].name, 'email': resS[0].email}
                }

                // Start the request
                request(options, function (error, response, body) {
                    if(error!=null)
                        res.status(500).send();
                    else
                        res.status(200).send();
                });
            }
        });

    }catch(e){
        errors.registerError(e);
        res.status(500).send({message: "Error inesperado"});
    }

});

//200: Todo OK
//401: No existe el usuario
//Esta ruta se accede desde android y manda el correo de verificación
router.post('/recoverAccount',(req,res)=>{
    try{
        var user = req.body.user;

        //Comprobar que existe el usuario
        db.query("SELECT name,email FROM user WHERE email = ? OR name = ?",[user,user],(err,resQ)=>{
            if(resQ.length==0)
                res.status(401).send();
            else{
                db.query("INSERT INTO aux_email_recover VALUES(?)",[resQ[0].email],(err,resI)=>{
                
                    //Mandar al otro servidor una petición para que mande el correo ya que digital ocean me da millones de problemas
                    var options = {
                        url: 'http://89.39.159.65:8090/recoverAccount',
                        method: 'POST',
                        form: {'email': resQ[0].email}
                    }

                    // Start the request
                    request(options, function (error, response, body) {
                        if(error!=null)
                            res.status(500).send();
                        else
                            res.status(200).send();
                    });
                })
            }
        });

    }catch(e){
        errors.registerError(e);
        res.status(500).send();
    }
});

//200: Todo OK
//401: No existe
//Esta ruta se abre desde el correo y muestra el formulario para restablecer la contraseña
router.get('/newpassword/:token',(req,res)=>{
    try{
        var email = authentication.decodeToken(req.params.token);

        if(email==null)
            res.status(500).render('error');
        else{

            db.query("SELECT * FROM aux_email_recover WHERE email = ?",[email],(err,resS)=>{
                if(resS.length==0)
                    res.status(404).render('error');
                else
                    res.status(200).render('newPassword',{email:email});
            });
        }

    }catch(e){
        errors.registerError(e);
        res.status(500).send({message: "Error inesperado"});
    }
});

//Esto es la respuesta del formulario para restablecer la contraseña
router.post('/newpassword',(req,res)=>{

    var password = req.body.password;
    var confirmPassword = req.body.passwordConfirm;
    var email = req.body.email;

    
    if(password!=confirmPassword)
        res.status(404).render('errorpassword');
    else{
        db.query("DELETE FROM aux_email_recover WHERE email = ?",[email],(err,resQ)=>{

            var encrypt = crypto.createHash('sha256').update(password).digest('base64');
            db.query("UPDATE user SET password = ? WHERE email = ?",[encrypt,email],(err,resU)=>{
                res.status(200).render('passwordchanged');
            });
        });

    }



});

module.exports = router;