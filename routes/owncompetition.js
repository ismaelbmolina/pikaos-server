var express = require('express');
var router = express.Router();
var db = require('../db');
var errors = require('../errors');
var authentication = require("../authentication");
var email = require("../email");
var forEach = require('async-foreach').forEach;

router.get('/test',(req,res)=>{
    res.status(200).send({saludo:"hola"});
});

//200: Competición cancelada
router.post('/cancell',(req,res)=>{
    var competition = req.body.competition;

    db.query("UPDATE competition SET state = 'cancelled' WHERE id = ?",[competition],(err,resQ)=>{
        if(err==null)
            res.status(200).send();
    });

});


//200: Jugadores y sus puntos (sólo para las ligas)
router.get('/table/:id',(req,res)=>{

    var idCompetition = req.params.id;

    db.query("select type from competition where id = ?",[idCompetition],(err,resC)=>{
        if(resC.length!=0 || err!=null){
            switch(resC[0].type){
                case "ind_league":
                    db.query("select u.name,l.points from ind_league_participant l join user u on l.user = u.id where l.league = ? order by points desc",[idCompetition],(err,resQ)=>{
                        res.status(200).send(resQ);
                    });
                    break;
                case "team_league":
                    db.query("select l.points, t.name from team_league_participant l join team t on l.team = t.id where league = ? order by points desc",[idCompetition],(err,resQ)=>{
                        res.status(200).send(resQ);
                    });
                    break;
                default:
                    res.status(400).send();
                    break;
            }
        }
    });
});

//200: Jugadores de una competición
router.get('/players/:id',(req,res)=>{
    var idCompetition = req.params.id;

    db.query("select type from competition where id = ?",[idCompetition],(err,resC)=>{
        if(resC.length!=0 || err!=null){
            switch(resC[0].type){
                case "ind_cup":
                    db.query("select u.name from ind_cup_participant ic join user u on ic.user = u.id  where cup = ?",[idCompetition],(err,resQ)=>{
                        res.status(200).send(resQ);
                    });
                    break;
                case "team_cup":
                        db.query("select t.name from team_cup_participant ic join team t on ic.team = t.id  where cup = ?",[idCompetition],(err,resQ)=>{
                            res.status(200).send(resQ);
                        });
                    break;
                case "ind_league":
                        db.query("select u.name from ind_league_participant ic join user u on ic.user = u.id  where league = ?",[idCompetition],(err,resQ)=>{
                            res.status(200).send(resQ);
                        });
                    break;
                case "team_league":
                        db.query("select t.name from team_league_participant ic join team t on ic.team = t.id  where league = ?",[idCompetition],(err,resQ)=>{
                            res.status(200).send(resQ);
                        });
                    break;
            }
        }
    });
});

//200: Todo OK
router.post('/winner',(req,res)=>{
    try{
        var token = req.headers.authorization;
        var competition = req.body.competition;
        var winner = req.body.winner;
        var round = req.body.round;
        var type = req.body.type;
        var playerone = req.body.playerone;
        var playertwo = req.body.playertwo;

        if(token==null)
            res.status(403).send({message: "Cabecera incorrecta, se requiere de un token"});
        else{
            switch(type){
                case "ind_cup":
                    db.query("UPDATE ind_cup_confrontation SET winner = ? WHERE id = ? AND nRound = ? AND player_one = ? AND player_two = ?",[winner,competition,round,playerone,playertwo],(err,resQ)=>{
                        res.status(200).send();
                    });
                    break;
                case "team_cup":
                    db.query("UPDATE team_cup_confrontation SET winner = ? WHERE id = ? AND nRound = ? AND team_one = ? AND team_two = ?",[winner,competition,round,playerone,playertwo],(err,resQ)=>{
                        res.status(200).send();
                    });
                    break;
                case "ind_league":
                    if(winner=="0"){
                        db.query("UPDATE ind_league_day_confrontation SET winner = ? WHERE id = ? AND league_day = ? AND player_one = ? AND player_two = ?",[winner,competition,round,playerone,playertwo],(err,resQ)=>{
                            res.status(200).send();
                        });
                    }else{
                        db.query("UPDATE ind_league_day_confrontation SET winner = ? WHERE id = ? AND league_day = ? AND player_one = ? AND player_two = ?",[winner,competition,round,playerone,playertwo],(err,resQ)=>{
                            res.status(200).send();
                        });
                    }
                    break;
                case "team_league":
                    if(winner=="0"){
                        db.query("UPDATE team_league_day_confrontation SET winner = ? WHERE id = ? AND league_day = ? AND team_one = ? AND team_two = ?",[winner,competition,round,playerone,playertwo],(err,resQ)=>{
                            res.status(200).send();
                        });
                    }else{
                        db.query("UPDATE team_league_day_confrontation SET winner = ? WHERE id = ? AND league_day = ? AND team_one = ? AND team_two = ?",[winner,competition,round,playerone,playertwo],(err,resQ)=>{
                            res.status(200).send();
                        });
                    }
                    break;
            }
        }
    
    }catch(e){
        errors.registerError(e);
        res.status(500).send({message: "Error inesperado"});
    }
})


//200: Todo OK -->
//      0: Comienza
//      1: Continua
//      2: Finaliza
//401: No hay jugadores para comenzar el enfrentamiento aun
//403: Todos los enfrentamientos deben estar resueltos (ganador establecido) antes de continuar a la siguiente ronda
router.post('/nextround',(req,res)=>{
    try{
        var token = req.headers.authorization;
        var competition = req.body.competition;

        if(token==null)
        res.status(403).send({message: "Cabecera incorrecta, se requiere de un token"});
        else{
            var userID = authentication.decodeToken(token);

            db.query("SELECT state,type FROM competition WHERE id = ?",[competition],(err,resQ)=>{
                //Comprobar que la competición exista
                if(err!=null || resQ.length==0)
                    res.status(500).send();
                else{
                    //Dependiendo del estado de la competición habrá que comenzarla o sólamente iniciar la siguiente ronda
                    if(resQ[0].state=="preparing"){

                        switch(resQ[0].type){
                        
                        case "ind_cup":
                            startIndCup(competition).then((done)=>{ 

                                if(done==null)
                                    res.status(401).send();
                                else
                                    res.status(200).send({response:0});

                            }).catch((rej)=>{
                                errors.registerError(rej);
                                res.status(500).send({message: "Error inesperado"});
                            });
                        break;

                        case "team_cup":
                            startTeamCup(competition).then((done)=>{ 

                                if(done==null)
                                    res.status(401).send();
                                else
                                    res.status(200).send({response:0});

                            }).catch((rej)=>{
                                errors.registerError(rej);
                                res.status(500).send({message: "Error inesperado"});
                            });
                        break;

                        case "ind_league":
                            startIndLeague(competition).then((done)=>{ 

                                if(done==null)
                                    res.status(401).send();
                                else
                                    res.status(200).send({response:0});

                            }).catch((rej)=>{
                                errors.registerError(rej);
                                res.status(500).send({message: "Error inesperado"});
                            });
                        break;

                        case "team_league":
                            startTeamLeague(competition).then((done)=>{ 

                                if(done==null)
                                    res.status(401).send();
                                else
                                    res.status(200).send({response:0});

                            }).catch((rej)=>{
                                errors.registerError(rej);
                                res.status(500).send({message: "Error inesperado"});
                            });
                        break;

                        }
                    }
                    
                    //COMPETING
                    if(resQ[0].state=="competing"){
                        switch(resQ[0].type){
                            case "ind_cup":
                                nextRoundIndCup(competition).then((done)=>{ 
                                    if(done==0)
                                        res.status(403).send();

                                    if(done==2)
                                        res.status(200).send({response:2});
                                    
                                    if(done==1)
                                        res.status(200).send({response:1});
                                }).catch((rej)=>{
                                    errors.registerError(rej);
                                    res.status(500).send({message: "Error inesperado"});
                                });
                            break;

                            case "team_cup":
                                nextRoundTeamCup(competition).then((done)=>{ 
                                    if(done==0)
                                        res.status(403).send();

                                    if(done==2)
                                        res.status(200).send({response:2});
                                    
                                    if(done==1)
                                        res.status(200).send({response:1});
                                }).catch((rej)=>{
                                    errors.registerError(rej);
                                    res.status(500).send({message: "Error inesperado"});
                                });
                            break;

                            case "ind_league":
                                nextRoundIndLeague(competition).then((done)=>{ 
                                    if(done==0)
                                        res.status(403).send();

                                    if(done==2)
                                        res.status(200).send({response:2});
                                    
                                    if(done==1)
                                        res.status(200).send({response:1});
                                }).catch((rej)=>{
                                    errors.registerError(rej);
                                    res.status(500).send({message: "Error inesperado"});
                                });
                            break;

                            case "team_league":
                                nextRoundTeamLeague(competition).then((done)=>{ 
                                    if(done==0)
                                        res.status(403).send();

                                    if(done==2)
                                        res.status(200).send({response:2});
                                    
                                    if(done==1)
                                        res.status(200).send({response:1});
                                }).catch((rej)=>{
                                    errors.registerError(rej);
                                    res.status(500).send({message: "Error inesperado"});
                                });
                            break;
                        }   
                    }
                }
            });
        }      
    }catch(e){
        errors.registerError(e);
        res.status(500).send({message: "Error inesperado"});
    }
});


//Siguiente ronda de la competición
//done 0: faltan jugadores por establecer como ganadores
//done 2: competición finalizada
//done 1: siguiente ronda establecida
function nextRoundIndCup(competition){
    return new Promise(function(done,rej){

        var winnersSetted = true;

        db.query("select * from ind_cup_confrontation where id = ? AND nRound = (select MAX(nRound) from ind_cup_confrontation where id = ?)",[competition,competition],(err,resConfrontations)=>{

            //Comprobamos que todos los enfrentamientos tienen un ganador establecido
            for (let index = 0; index < resConfrontations.length; index++) {
                const confrontation = resConfrontations[index];

                if(confrontation.winner==null)
                    winnersSetted = false;
            }
          
            if(!winnersSetted)
                done(0);
            else{

                //Si la competición está en su última ronda la finalizamos
                db.query("select MAX(r.nRound) as actualRound, c.nRounds from ind_cup_round r join ind_cup c on r.id = c.id where r.id = ?",[competition],(err,resRounds)=>{
                    

                    if(resRounds[0].actualRound==resRounds[0].nRounds){
                        db.query("UPDATE competition SET state = 'finished' WHERE id = ?",[competition],(err,resU)=>{
                            done(2);
                        });
                    }else{
                        //console.log(resRounds[0].actualRound);
                        var newRound = resRounds[0].actualRound + 1;
                        //En caso de que no esté creamos la siguiente ronda
                        db.query("INSERT INTO ind_cup_round VALUES(?,?)",[competition,newRound],(err,resI)=>{

                            if(err!=null)
                                console.log("error con la inserccion de ronda");

                            //Creamos los enfrentamientos
                            var confrontations = []

                            for (let index = 0; index < resConfrontations.length; index++) {
                                var tmpConfrontation = resConfrontations[index];
                                var playerOne = null;
                                var playerTwo = null;

                                playerOne = tmpConfrontation.winner;

                                index++;

                                if(index<resConfrontations.length){
                                    tmpConfrontation = resConfrontations[index];

                                    playerTwo = tmpConfrontation.winner;
                                }else
                                    playerTwo = "0";

                                var confrontation = {
                                    pOne: playerOne,
                                    pTwo: playerTwo
                                }

                                confrontations.push(confrontation);
                            }

                            querys = [];

                            for (let index = 0; index < confrontations.length; index++) {
                                var confrontation = confrontations[index];

                                querys.push(insertConfrontations("ind_cup_confrontation",competition,newRound,confrontation.pOne,confrontation.pTwo));   
                            }

                            Promise.all(querys).then((don)=>{
                                done(1);
                            });
                        }); 
                    }
                });
            }

        });
    });
}


//Siguiente ronda de la competición
//done 0: faltan jugadores por establecer como ganadores
//done 2: competición finalizada
//done 1: siguiente ronda establecida
function nextRoundTeamCup(competition){
    return new Promise(function(done,rej){

        var winnersSetted = true;

        db.query("select * from team_cup_confrontation where id = ? AND nRound = (select MAX(nRound) from team_cup_confrontation where id = ?)",[competition,competition],(err,resConfrontations)=>{

            //Comprobamos que todos los enfrentamientos tienen un ganador establecido
            for (let index = 0; index < resConfrontations.length; index++) {
                const confrontation = resConfrontations[index];

                if(confrontation.winner==null)
                    winnersSetted = false;
            }
          
            if(!winnersSetted)
                done(0);
            else{

                //Si la competición está en su última ronda la finalizamos
                db.query("select MAX(r.nRound) as actualRound, c.nRounds from team_cup_round r join team_cup c on r.id = c.id where r.id = ?",[competition],(err,resRounds)=>{
                    

                    if(resRounds[0].actualRound==resRounds[0].nRounds){
                        db.query("UPDATE competition SET state = 'finished' WHERE id = ?",[competition],(err,resU)=>{
                            done(2);
                        });
                    }else{
                        //console.log(resRounds[0].actualRound);
                        var newRound = resRounds[0].actualRound + 1;
                        //En caso de que no esté creamos la siguiente ronda
                        db.query("INSERT INTO team_cup_round VALUES(?,?)",[competition,newRound],(err,resI)=>{

                            if(err!=null)
                                console.log("error con la inserccion de ronda");

                            //Creamos los enfrentamientos
                            var confrontations = []

                            for (let index = 0; index < resConfrontations.length; index++) {
                                var tmpConfrontation = resConfrontations[index];
                                var playerOne = null;
                                var playerTwo = null;

                                playerOne = tmpConfrontation.winner;

                                index++;

                                if(index<resConfrontations.length){
                                    tmpConfrontation = resConfrontations[index];

                                    playerTwo = tmpConfrontation.winner;
                                }else
                                    playerTwo = "0";

                                var confrontation = {
                                    pOne: playerOne,
                                    pTwo: playerTwo
                                }

                                confrontations.push(confrontation);
                            }

                            querys = [];

                            for (let index = 0; index < confrontations.length; index++) {
                                var confrontation = confrontations[index];

                                querys.push(insertConfrontations("team_cup_confrontation",competition,newRound,confrontation.pOne,confrontation.pTwo));   
                            }

                            Promise.all(querys).then((don)=>{
                                done(1);
                            });
                        }); 
                    }
                });
            }

        });
    });
}


//Siguiente ronda de la competición
//done 0: faltan jugadores por establecer como ganadores
//done 2: competición finalizada
//done 1: siguiente ronda establecida
function nextRoundIndLeague(competition){
    return new Promise(function(done,rej){

        var winnersSetted = true;

        db.query("select * from ind_league_day_confrontation where id = ? AND league_day = (select MAX(league_day) from  ind_league_day_confrontation where id = ?)",[competition,competition],(err,resConfrontations)=>{

            //Comprobamos que todos los enfrentamientos tienen un ganador establecido
            for (let index = 0; index < resConfrontations.length; index++) {
                const confrontation = resConfrontations[index];

                if(confrontation.winner==null)
                    winnersSetted = false;
            }
          
            if(!winnersSetted)
                done(0);
            else{

                var querysPoints = []
                //Establecemos los puntos
                for (let index = 0; index < resConfrontations.length; index++) {
                    const confrontation = resConfrontations[index];

                    //Esto significa que es un empate
                    if(confrontation.winner=="0"){
                        querysPoints.push(setPointsIndLeague(competition,confrontation.player_one,1));
                        querysPoints.push(setPointsIndLeague(competition,confrontation.player_two,1));
                    }else
                        querysPoints.push(setPointsIndLeague(competition,confrontation.winner,3));
                }

                
                Promise.all(querysPoints).then((donePoints)=>{

                    //Si la competición está en su última ronda la finalizamos
                    db.query("select MAX(r.league_day) as actualRound, c.nLeague_days from ind_league_day r join ind_league c on r.id = c.id where r.id = ?",[competition],(err,resRounds)=>{
                        

                        if(resRounds[0].actualRound==resRounds[0].nLeague_days){
                            db.query("UPDATE competition SET state = 'finished' WHERE id = ?",[competition],(err,resU)=>{
                                done(2);
                            });
                        }else{

                            var newRound = resRounds[0].actualRound + 1;
                            //En caso de que no esté creamos la siguiente ronda
                            db.query("INSERT INTO ind_league_day VALUES(?,?)",[competition,newRound],(err,resI)=>{

                                //ENFRENTAMIENTOS
                                db.query(" select u.name from ind_league_participant i join user u on i.user = u.id where i.league = ?  order by i.user asc",[competition],(err,resPlayers)=>{

                                    //Añado un última posición auxiliar en caso de partidas con jugadores impares
                                    if(resPlayers.length % 2 !=0)
                                        resPlayers.push({name:"0"});


                                    //Establecer la posición del array newPlayersOrder dependiendo la ronda actual
                                    var newPlayersOrder  = [];

                                    //El jugador uno lo añadimos siempre
                                    newPlayersOrder.push(resPlayers[0].name);

                                    //Nuevo orden para los emparejamientos
                                    for (let index = 1; index < resPlayers.length; index++) {
                                        
                                        var actualPlayer = resPlayers[index].name;

                                        var newPosition = index + resRounds[0].actualRound;//Aunque debería ser la ronda actual - 1 , ya que el array empieza por 0 y las rondas por 1, la ronda actual realmente es la anterior ya que se añadió una

                                        if(newPosition < resPlayers.length)
                                            newPlayersOrder[newPosition] = actualPlayer;
                                        else{
                                            var newPositionFixed = newPosition - resPlayers.length + 1;
                                            newPlayersOrder[newPositionFixed] = actualPlayer;
                                        }
                                    }


                                    var close = false;
                                    var confrontations = [];

                                    //CREAR ENFRENTAMIENTOS primero con el ultimo,segundo con penultimo, etc...
                                    var firstIndex = 0;
                                    var lastIndex = newPlayersOrder.length - 1;

                                    while(!close){

                                        var participantOne = null;
                                        var participantTwo = null;


                                        if(firstIndex>lastIndex){
                                            close = true;
                                        }else{
                                            participantOne = newPlayersOrder[firstIndex];
                                            participantTwo = newPlayersOrder[lastIndex];
                                        }

                                        firstIndex++;
                                        lastIndex--;

                                        if(close!=true){
                                            //Añadimos el nuevo enfrentamiento al array
                                            var confrontation = {
                                                one: participantOne,
                                                two: participantTwo
                                            }

                                            confrontations.push(confrontation);
                                        }
                                    }

                                    console.log(confrontations);

                                    querys = [];

                                    for (let index = 0; index < confrontations.length; index++) {
                                        var confrontation = confrontations[index];

                                        querys.push(insertConfrontations("ind_league_day_confrontation",competition,newRound,confrontation.one,confrontation.two));   
                                    }

                                    Promise.all(querys).then((don)=>{
                                        done(1);
                                    });
                                });
                            });
                        }
                    });
                });
            }
        });
    });
}


//Siguiente ronda de la competición
//done 0: faltan jugadores por establecer como ganadores
//done 2: competición finalizada
//done 1: siguiente ronda establecida
function nextRoundTeamLeague(competition){
    return new Promise(function(done,rej){

        var winnersSetted = true;

        db.query("select * from team_league_day_confrontation where id = ? AND league_day = (select MAX(league_day) from  team_league_day_confrontation where id = ?)",[competition,competition],(err,resConfrontations)=>{

            //Comprobamos que todos los enfrentamientos tienen un ganador establecido
            for (let index = 0; index < resConfrontations.length; index++) {
                const confrontation = resConfrontations[index];

                if(confrontation.winner==null)
                    winnersSetted = false;
            }
          
            if(!winnersSetted)
                done(0);
            else{

                var querysPoints = []
                //Establecemos los puntos
                for (let index = 0; index < resConfrontations.length; index++) {
                    const confrontation = resConfrontations[index];

                    //Esto significa que es un empate
                    if(confrontation.winner=="0"){
                        querysPoints.push(setPointsTeamLeague(competition,confrontation.team_one,1));
                        querysPoints.push(setPointsTeamLeague(competition,confrontation.team_two,1));
                    }else
                        querysPoints.push(setPointsTeamLeague(competition,confrontation.winner,3));
                }

                
                Promise.all(querysPoints).then((donePoints)=>{

                    //Si la competición está en su última ronda la finalizamos
                    db.query("select MAX(r.league_day) as actualRound, c.nLeague_days from team_league_day r join team_league c on r.id = c.id where r.id = ?",[competition],(err,resRounds)=>{
                        

                        if(resRounds[0].actualRound==resRounds[0].nLeague_days){
                            db.query("UPDATE competition SET state = 'finished' WHERE id = ?",[competition],(err,resU)=>{
                                done(2);
                            });
                        }else{

                            var newRound = resRounds[0].actualRound + 1;
                            //En caso de que no esté creamos la siguiente ronda
                            db.query("INSERT INTO team_league_day VALUES(?,?)",[competition,newRound],(err,resI)=>{

                                //ENFRENTAMIENTOS
                                db.query("select u.name from team_league_participant i join team u on i.team = u.id where i.league = ?  order by i.team asc",[competition],(err,resPlayers)=>{

                                    //Añado un última posición auxiliar en caso de partidas con jugadores impares
                                    if(resPlayers.length % 2 !=0)
                                        resPlayers.push({name:"0"});


                                    //Establecer la posición del array newPlayersOrder dependiendo la ronda actual
                                    var newPlayersOrder  = [];

                                    //El jugador uno lo añadimos siempre
                                    newPlayersOrder.push(resPlayers[0].name);

                                    //Nuevo orden para los emparejamientos
                                    for (let index = 1; index < resPlayers.length; index++) {
                                        
                                        var actualPlayer = resPlayers[index].name;

                                        var newPosition = index + resRounds[0].actualRound;//Aunque debería ser la ronda actual - 1 , ya que el array empieza por 0 y las rondas por 1, la ronda actual realmente es la anterior ya que se añadió una

                                        if(newPosition < resPlayers.length)
                                            newPlayersOrder[newPosition] = actualPlayer;
                                        else{
                                            var newPositionFixed = newPosition - resPlayers.length + 1;
                                            newPlayersOrder[newPositionFixed] = actualPlayer;
                                        }
                                    }


                                    var close = false;
                                    var confrontations = [];

                                    //CREAR ENFRENTAMIENTOS primero con el ultimo,segundo con penultimo, etc...
                                    var firstIndex = 0;
                                    var lastIndex = newPlayersOrder.length - 1;

                                    while(!close){

                                        var participantOne = null;
                                        var participantTwo = null;


                                        if(firstIndex>lastIndex){
                                            close = true;
                                        }else{
                                            participantOne = newPlayersOrder[firstIndex];
                                            participantTwo = newPlayersOrder[lastIndex];
                                        }

                                        firstIndex++;
                                        lastIndex--;

                                        if(close!=true){
                                            //Añadimos el nuevo enfrentamiento al array
                                            var confrontation = {
                                                one: participantOne,
                                                two: participantTwo
                                            }

                                            confrontations.push(confrontation);
                                        }
                                    }

                                    console.log(confrontations);

                                    querys = [];

                                    for (let index = 0; index < confrontations.length; index++) {
                                        var confrontation = confrontations[index];

                                        querys.push(insertConfrontations("team_league_day_confrontation",competition,newRound,confrontation.one,confrontation.two));   
                                    }

                                    Promise.all(querys).then((don)=>{
                                        done(1);
                                    });
                                });
                            });
                        }
                    });
                });
            }
        });
    });
}



function insertConfrontations(table,competition,newRound,playerOne,playerTwo){

    return new Promise(function(don,rej){
        
        var winner = null;

        if(playerTwo=="0")
            winner = playerOne;

        if(playerOne=="0")
            winner = playerTwo;

        db.query("INSERT INTO " + table + " VALUES(?,?,?,?,?)",[competition,newRound,playerOne,playerTwo,winner],(err,resI)=>{
            if(err!=null)
                console.log(err);
            don(resI);
        });
    });
}

function setPointsIndLeague(competition,player,addPoints){
    return new Promise(function(don,rej){
        db.query("select points from ind_league_participant where user = (select id from user where name = ?) AND league = ?",[player,competition],(err,resPoints)=>{
            if(resPoints.length>0){
                var actualPoints = resPoints[0].points;
                var newPoints = actualPoints+addPoints;

                db.query("UPDATE ind_league_participant SET points = ? WHERE league = ? AND user = (select id from user where name = ?)",[newPoints,competition,player],(err,resU)=>{
                    don(resU);
                });
            }
        });
    });
}

function setPointsTeamLeague(competition,team,addPoints){
    return new Promise(function(don,rej){
        db.query("select points from team_league_participant where team = (select id from team where name = ?) AND league = ?",[team,competition],(err,resPoints)=>{
            if(resPoints.length>0){
                var actualPoints = resPoints[0].points;
                var newPoints = actualPoints+addPoints;

                db.query("UPDATE team_league_participant SET points = ? WHERE league = ? AND team = (select id from team where name = ?)",[newPoints,competition,team],(err,resU)=>{
                    don(resU);
                });
            }
        });
    });
}

//La competición es iniciada por primera vez
//En caso de que no haya jugadores suficientes devuelve null
function startIndCup(competition){
    return new Promise(function(done,rej){
        //Comprobamos que hay jugadores suficients
        db.query(" select ic.user,u.name from ind_cup_participant ic join user u on ic.user = u.id  where cup = ?",[competition],(err,players)=>{
            if(players.length<3)
                done(null);
            else{
                //PONEMOS LA COMPETICIÓN A ESTADO "COMPITIENDO"
                db.query("UPDATE competition SET state = 'competing' WHERE id = ?",[competition],(err,resU)=>{

                    //ACTUALIZAMOS EL NÚMERO DE RONDAS QUE TENDRÁ
                    var nRoundsDecimal = Math.log2(players.length);
                    var nRounds = Math.round(nRoundsDecimal);

                    if(nRoundsDecimal>nRounds){
                        nRounds++;
                    }

                    db.query("UPDATE ind_cup SET nRounds = ? WHERE id = ?",[nRounds,competition],(err,resUnRounds)=>{


                        //CREAMOS LA PRIMERA RONDA
                        db.query("INSERT INTO ind_cup_round VALUES(?,1)",[competition],(err,resI)=>{

                            var close = false;
                            var confrontations = [];


                            //CREAR ENFRENTAMIENTOS DE FORMA ALEATORIA
                            while(!close){

                                var participantOne = null;
                                var participantTwo = null;

                                if(players.length==0)
                                    close = true;
                                else{
                                    //Calculamos una posición de participante aleatoria lo guardamos
                                    var index = Math.floor(Math.random() * players.length);
                                    participantOne = players[index].name;

                                    //Borramos del array la posición obtenida
                                    players.splice(index, 1);
                                }


                                if(players.length==0)
                                    close = true;
                                else{
                                    var index = Math.floor(Math.random() * players.length);
                                    participantTwo = players[index].name;

                                    //Borramos del array la posición obtenida
                                    players.splice(index, 1);
                                }


                                //Añadimos el nuevo enfrentamiento al array
                                if(participantOne!=null){
                                    var confrontation = {
                                        one: participantOne,
                                        two: participantTwo
                                    }

                                    confrontations.push(confrontation);
                                }
                            }


                            //REGISTRAR LOS ENFRENTAMIENTOS EN LA BBDD
                            forEach(confrontations, function(item, index, arr) {

                                playerOne = item.one;
                                playerTwo = item.two;

                                //En la BBDD que un enfrentamiento este cojo se añade un 0, (problemas que me da con los nulos, para evitarlo, desde android recogeré el 0)
                                if(playerTwo==null){
                                    playerTwo = "0";
                                    db.query("INSERT INTO ind_cup_confrontation VALUES(?,1,?,?,?)",[competition,playerOne,playerTwo,playerOne]);
                                }
                                else{
                                    db.query("INSERT INTO ind_cup_confrontation VALUES(?,1,?,?,NULL)",[competition,playerOne,playerTwo]);
                                }
                            });

                            done("0");
                        });
                    }); 
                });
            }
        });
    });
}

//La competición es iniciada por primera vez
//En caso de que no haya jugadores suficientes devuelve null
function startTeamCup(competition){
    return new Promise(function(done,rej){
        //Comprobamos que hay jugadores suficients
        db.query(" select ic.team,u.name from team_cup_participant ic join team u on ic.team = u.id  where cup = ?",[competition],(err,players)=>{
            if(players.length<3)
                done(null);
            else{
                //PONEMOS LA COMPETICIÓN A ESTADO "COMPITIENDO"
                db.query("UPDATE competition SET state = 'competing' WHERE id = ?",[competition],(err,resU)=>{

                    //ACTUALIZAMOS EL NÚMERO DE RONDAS QUE TENDRÁ
                    var nRoundsDecimal = Math.log2(players.length);
                    var nRounds = Math.round(nRoundsDecimal);

                    if(nRoundsDecimal>nRounds){
                        nRounds++;
                    }

                    db.query("UPDATE team_cup SET nRounds = ? WHERE id = ?",[nRounds,competition],(err,resUnRounds)=>{


                        //CREAMOS LA PRIMERA RONDA
                        db.query("INSERT INTO team_cup_round VALUES(?,1)",[competition],(err,resI)=>{

                            var close = false;
                            var confrontations = [];


                            //CREAR ENFRENTAMIENTOS DE FORMA ALEATORIA
                            while(!close){

                                var participantOne = null;
                                var participantTwo = null;

                                if(players.length==0)
                                    close = true;
                                else{
                                    //Calculamos una posición de participante aleatoria lo guardamos
                                    var index = Math.floor(Math.random() * players.length);
                                    participantOne = players[index].name;

                                    //Borramos del array la posición obtenida
                                    players.splice(index, 1);
                                }


                                if(players.length==0)
                                    close = true;
                                else{
                                    var index = Math.floor(Math.random() * players.length);
                                    participantTwo = players[index].name;

                                    //Borramos del array la posición obtenida
                                    players.splice(index, 1);
                                }


                                //Añadimos el nuevo enfrentamiento al array
                                if(participantOne!=null){
                                    var confrontation = {
                                        one: participantOne,
                                        two: participantTwo
                                    }

                                    confrontations.push(confrontation);
                                }
                            }


                            //REGISTRAR LOS ENFRENTAMIENTOS EN LA BBDD
                            forEach(confrontations, function(item, index, arr) {

                                playerOne = item.one;
                                playerTwo = item.two;

                                //En la BBDD que un enfrentamiento este cojo se añade un 0, (problemas que me da con los nulos, para evitarlo, desde android recogeré el 0)
                                if(playerTwo==null){
                                    playerTwo = "0";
                                    db.query("INSERT INTO team_cup_confrontation VALUES(?,1,?,?,?)",[competition,playerOne,playerTwo,playerOne]);
                                }
                                else{
                                    db.query("INSERT INTO team_cup_confrontation VALUES(?,1,?,?,NULL)",[competition,playerOne,playerTwo]);
                                }
                            });

                            done("0");
                        });
                    }); 
                });
            }
        });
    });
}

//La competición es iniciada por primera vez
//En caso de que no haya jugadores suficientes devuelve null
function startIndLeague(competition){
    return new Promise(function(done,rej){
        //Comprobamos que hay jugadores suficients
        db.query("select ic.user,u.name from ind_league_participant ic join user u on ic.user = u.id where league = ? order by ic.user asc",[competition],(err,players)=>{
            if(players.length<3)
                done(null);
            else{
                //PONEMOS LA COMPETICIÓN A ESTADO "COMPITIENDO"
                db.query("UPDATE competition SET state = 'competing' WHERE id = ?",[competition],(err,resU)=>{

                    //ACTUALIZAMOS EL NÚMERO DE RONDAS QUE TENDRÁ
                    var rest = players.length % 2;
                    var nRounds;
                    
                    if(rest==1)
                        nRounds = players.length;
                    else
                        nRounds = players.length -1;

                    db.query("UPDATE ind_league SET nLeague_days = ? WHERE id = ?",[nRounds,competition],(err,resUnRounds)=>{


                        //CREAMOS LA PRIMERA RONDA
                        db.query("INSERT INTO ind_league_day VALUES(?,1)",[competition],(err,resI)=>{

                            //Añado un última posición auxiliar en caso de partidas con jugadores impares
                            if(players.length % 2 !=0)
                            players.push({name:"0"});


                            //Establecer la posición del array newPlayersOrder dependiendo la ronda actual
                            var newPlayersOrder  = [];

                            //El jugador uno lo añadimos siempre
                            newPlayersOrder.push(players[0].name);

                            //Nuevo orden para los emparejamientos
                            for (let index = 1; index < players.length; index++) {
                                
                                var actualPlayer = players[index].name;

                                var newPosition = index+0;//Aunque debería ser la ronda actual - 1 , ya que el array empieza por 0 y las rondas por 1, la ronda actual realmente es la anterior ya que se añadió una

                                if(newPosition < players.length)
                                    newPlayersOrder[newPosition] = actualPlayer;
                                else{
                                    var newPositionFixed = newPosition - players.length + 1;
                                    newPlayersOrder[newPositionFixed] = actualPlayer;
                                }
                            }


                            var close = false;
                            var confrontations = [];

                            //CREAR ENFRENTAMIENTOS primero con el ultimo,segundo con penultimo, etc...
                            var firstIndex = 0;
                            var lastIndex = newPlayersOrder.length - 1;

                            while(!close){

                                var participantOne = null;
                                var participantTwo = null;


                                if(firstIndex>lastIndex){
                                    close = true;
                                }else{
                                    participantOne = newPlayersOrder[firstIndex];
                                    participantTwo = newPlayersOrder[lastIndex];
                                }

                                firstIndex++;
                                lastIndex--;

                                if(close!=true){
                                    //Añadimos el nuevo enfrentamiento al array
                                    var confrontation = {
                                        one: participantOne,
                                        two: participantTwo
                                    }

                                    confrontations.push(confrontation);
                                }
                            }

                            console.log(confrontations);


                            //REGISTRAR LOS ENFRENTAMIENTOS EN LA BBDD
                            forEach(confrontations, function(item, index, arr) {

                                playerOne = item.one;
                                playerTwo = item.two;

                                var winner = null;

                                if(playerTwo=="0")
                                    winner = playerOne;

                                if(playerOne=="0")
                                    winner = playerTwo;

                                db.query("INSERT INTO ind_league_day_confrontation VALUES(?,1,?,?,?)",[competition,playerOne,playerTwo,winner]);
                            });

                            done("0");
                        });
                    }); 
                });
            }
        });
    });
}

//La competición es iniciada por primera vez
//En caso de que no haya jugadores suficientes devuelve null
function startTeamLeague(competition){
    return new Promise(function(done,rej){
        //Comprobamos que hay jugadores suficients
        db.query("select ic.team,u.name from team_league_participant ic join team u on ic.team = u.id where league = ? order by ic.team asc",[competition],(err,players)=>{
            if(players.length<3)
                done(null);
            else{
                //PONEMOS LA COMPETICIÓN A ESTADO "COMPITIENDO"
                db.query("UPDATE competition SET state = 'competing' WHERE id = ?",[competition],(err,resU)=>{

                    //ACTUALIZAMOS EL NÚMERO DE RONDAS QUE TENDRÁ
                    var rest = players.length % 2;
                    var nRounds;
                    
                    if(rest==1)
                        nRounds = players.length;
                    else
                        nRounds = players.length -1;

                    db.query("UPDATE team_league SET nLeague_days = ? WHERE id = ?",[nRounds,competition],(err,resUnRounds)=>{


                        //CREAMOS LA PRIMERA RONDA
                        db.query("INSERT INTO team_league_day VALUES(?,1)",[competition],(err,resI)=>{

                            //Añado un última posición auxiliar en caso de partidas con jugadores impares
                            if(players.length % 2 !=0)
                            players.push({name:"0"});


                            //Establecer la posición del array newPlayersOrder dependiendo la ronda actual
                            var newPlayersOrder  = [];

                            //El jugador uno lo añadimos siempre
                            newPlayersOrder.push(players[0].name);

                            //Nuevo orden para los emparejamientos
                            for (let index = 1; index < players.length; index++) {
                                
                                var actualPlayer = players[index].name;

                                var newPosition = index+0;//Aunque debería ser la ronda actual - 1 , ya que el array empieza por 0 y las rondas por 1, la ronda actual realmente es la anterior ya que se añadió una

                                if(newPosition < players.length)
                                    newPlayersOrder[newPosition] = actualPlayer;
                                else{
                                    var newPositionFixed = newPosition - players.length + 1;
                                    newPlayersOrder[newPositionFixed] = actualPlayer;
                                }
                            }


                            var close = false;
                            var confrontations = [];

                            //CREAR ENFRENTAMIENTOS primero con el ultimo,segundo con penultimo, etc...
                            var firstIndex = 0;
                            var lastIndex = newPlayersOrder.length - 1;

                            while(!close){

                                var participantOne = null;
                                var participantTwo = null;


                                if(firstIndex>lastIndex){
                                    close = true;
                                }else{
                                    participantOne = newPlayersOrder[firstIndex];
                                    participantTwo = newPlayersOrder[lastIndex];
                                }

                                firstIndex++;
                                lastIndex--;

                                if(close!=true){
                                    //Añadimos el nuevo enfrentamiento al array
                                    var confrontation = {
                                        one: participantOne,
                                        two: participantTwo
                                    }

                                    confrontations.push(confrontation);
                                }
                            }

                            console.log(confrontations);


                            //REGISTRAR LOS ENFRENTAMIENTOS EN LA BBDD
                            forEach(confrontations, function(item, index, arr) {

                                playerOne = item.one;
                                playerTwo = item.two;

                                var winner = null;

                                if(playerTwo=="0")
                                    winner = playerOne;

                                if(playerOne=="0")
                                    winner = playerTwo;

                                db.query("INSERT INTO team_league_day_confrontation VALUES(?,1,?,?,?)",[competition,playerOne,playerTwo,winner]);
                            });

                            done("0");
                        });
                    }); 
                });
            }
        });
    });
}








//200: Todo OK
router.post('/report',(req,res)=>{
    try{
        var token = req.headers.authorization;
        var competition = req.body.idCompetition;
        var description = req.body.description;

        if(token==null)
            res.status(403).send({message: "Cabecera incorrecta, se requiere de un token"});
        else{
        
            var userID = authentication.decodeToken(token);
            email.reportAdmin(userID,competition,description);
            res.status(200).send();

        }
        
    }catch(e){
        errors.registerError(e);
        res.status(500).send({message: "Error inesperado"});
    }
});


//200: TODO OK
router.post('/description',(req,res)=>{
    try{
        var competition = req.body.idCompetition;
        var description = req.body.description;

        console.log(competition);
        console.log(description);

        db.query("UPDATE competition SET description = ? WHERE id = ?",[description,competition],(err,resQ)=>{
            res.status(200).send();
        });

    }catch(e){
        errors.registerError(e);
        res.status(500).send();
    }

});


//Parámetros entrada: indcup, indleague, teamleague y teamcup para filtrar que se quiere
//200: Json con las competiciones que ya participa el usuario
router.get('/', function (req, res) {

    try{
        var token = req.headers.authorization;
        if(token==null)
            res.status(403).send({message: "Cabecera incorrecta, se requiere de un token"});
        else{
        
            var userID = authentication.decodeToken(token);

            var querys =[];

            if(req.query.indcup!=null)
                querys.push(getIndividualCupsJoined(userID));
            if(req.query.indleague!=null)
                querys.push(getIndividualLeaguesJoined(userID));
            if(req.query.teamcup!=null)
                querys.push(getTeamCupsJoined(userID));
            if(req.query.teamleague!=null)
                querys.push(getTeamLeaguesJoined(userID));

            Promise.all(querys).then((result)=>{

                //Se discrimina si no se encontró ningún dato tanto si no lo hay como si se buscó nada y además
                //los datos encontrados se depuran un poco para que queden bien
                var json;
                if(result.length>0) {
                    
                    json = result[0];

                    for (let index = 1; index < result.length; index++) {
                        json = json.concat(result[index]);      
                    }
                }else{
                    json = [];
                }
            
                res.status(200).send(json);

            }).catch((rej)=>{
                errors.registerError(rej);
                res.status(500).send({message: "Error inesperado"});
            });
        }
    }catch(e){
        errors.registerError(e);
        res.status(500).send({message: "Error inesperado"});
    }
});

//200: TODO OK
router.get('/info/:id',(req,res)=>{
    var idCompetition = req.params.id;
    db.query("select id, type from competition c where id = ?", [idCompetition],(err,resQInfo)=>{

        if(resQInfo.length!=0){

            switch(resQInfo[0].type){
                case "ind_cup":
                    getCupIndividualInfo(idCompetition).then((done)=>{ 
                        res.status(200).send(done);
                    }).catch((rej)=>{
                        errors.registerError(rej);
                        res.status(500).send({message: "Error inesperado"});
                    });
                    break;

                case "ind_league":
                    getLeagueIndividualInfo(idCompetition).then((done)=>{
                        res.status(200).send(done);
                    }).catch((rej)=>{
                        errors.registerError(rej);
                        res.status(500).send({message: "Error inesperado"});
                    })
                    break;

                case "team_cup":
                    getCupTeamlInfo(idCompetition).then((done)=>{
                        res.status(200).send(done);
                    }).catch((rej)=>{
                        errors.registerError(rej);
                        res.status(500).send({message: "Error inesperado"});
                    })
                    break;
                case "team_league":
                    getLeagueTeamInfo(idCompetition).then((done)=>{
                        res.status(200).send(done);
                    }).catch((rej)=>{
                        errors.registerError(rej);
                        res.status(500).send({message: "Error inesperado"});
                    })
                    break;
            }
    }

    });
});



//INFO
function getLeagueTeamInfo (id) {
    return new Promise(function(done,rej){
        //select c.id, c.league_day, c.player_one,c.player_two,c.winner from ind_league_day_confrontation c join ind_league_day r on r.id = c.id and r.league_day = c.league_day where r.id = 2 order by c.league_day;
        db.query("select c.id, c.league_day, c.team_one,c.team_two,c.winner from team_league_day_confrontation c join team_league_day r on r.id = c.id and r.league_day = c.league_day where r.id = ? order by c.league_day",
        [id],(err,resQ)=>{

            if(err!=null)
                rej(err);
 
            var json = [];
            var confrontations = [];


            for (let index = 0; index < resQ.length; index++) {
                const line = resQ[index];

                //Obtenemos los datos de la confrontación de cada fila y lo añadimos al array de confrontations
                var confrontation = {
                    player_one: line.team_one,
                    player_two: line.team_two,
                    winner: line.winner,
                };

                confrontations.push(confrontation);

                //Comprobamos si el número de ronda actual es la última vez que aparece o si es la última linea directamente
                //En este caso añadimos la ronda con las confrontacioens recogidas hasta ahora

                if(index == resQ.length-1 || line.league_day != resQ[index+1].league_day){
                    var round = {
                        nRound: line.league_day,
                        confrontations: confrontations
                    }

                    //Añadimos la ronda al array respuesta
                    json.push(round);

                    //Limpiamos las confrontaciones de esta ronda que ya quedo registrada
                    confrontations = [];
                }
            }

            done(json);
        });
    });
}

function getCupTeamlInfo (id){
    return new Promise(function(done,rej){
        //select c.id,c.nRound, r.round_name,c.player_one,c.player_two,c.winner from ind_cup_confrontation c join ind_cup_round r on r.id = c.id and r.nRound = c.nRound where r.id = 1;
        db.query("select c.id,c.nRound,c.team_one,c.team_two,c.winner from team_cup_confrontation c join team_cup_round r on r.id = c.id and r.nRound = c.nRound where r.id = ? order by c.nRound",
        [id],(err,resQ)=>{

            if(err!=null)
                rej(err);

            
            var json = [];
            var confrontations = [];


            for (let index = 0; index < resQ.length; index++) {
                const line = resQ[index];

                //Obtenemos los datos de la confrontación de cada fila y lo añadimos al array de confrontations
                var confrontation = {
                    player_one: line.team_one,
                    player_two: line.team_two,
                    winner: line.winner,
                };

                confrontations.push(confrontation);

                //Comprobamos si el número de ronda actual es la última vez que aparece o si es la última linea directamente
                //En este caso añadimos la ronda con las confrontacioens recogidas hasta ahora


                if(index == resQ.length-1 || line.nRound != resQ[index+1].nRound){
                    var round = {
                        nRound: line.nRound,
                        confrontations: confrontations
                    }

                    //Añadimos la ronda al array respuesta
                    json.push(round);

                    //Limpiamos las confrontaciones de esta ronda que ya quedo registrada
                    confrontations = [];
                }
            }

            done(json);
        });
    });
}

function getLeagueIndividualInfo (id) {
    return new Promise(function(done,rej){
        //select c.id, c.league_day, c.player_one,c.player_two,c.winner from ind_league_day_confrontation c join ind_league_day r on r.id = c.id and r.league_day = c.league_day where r.id = 2 order by c.league_day;
        db.query("select c.id, c.league_day, c.player_one,c.player_two,c.winner from ind_league_day_confrontation c join ind_league_day r on r.id = c.id and r.league_day = c.league_day where r.id = ? order by c.league_day",
        [id],(err,resQ)=>{

            if(err!=null)
                rej(err);
 
            var json = [];
            var confrontations = [];


            for (let index = 0; index < resQ.length; index++) {
                const line = resQ[index];

                //Obtenemos los datos de la confrontación de cada fila y lo añadimos al array de confrontations
                var confrontation = {
                    player_one: line.player_one,
                    player_two: line.player_two,
                    winner: line.winner,
                };

                confrontations.push(confrontation);

                //Comprobamos si el número de ronda actual es la última vez que aparece o si es la última linea directamente
                //En este caso añadimos la ronda con las confrontacioens recogidas hasta ahora

                if(index == resQ.length-1 || line.league_day != resQ[index+1].league_day){
                    var round = {
                        nRound: line.league_day,
                        confrontations: confrontations
                    }

                    //Añadimos la ronda al array respuesta
                    json.push(round);

                    //Limpiamos las confrontaciones de esta ronda que ya quedo registrada
                    confrontations = [];
                }
            }

            done(json);
        });
    });
}

function getCupIndividualInfo (id){
    return new Promise(function(done,rej){
        //select c.id,c.nRound, r.round_name,c.player_one,c.player_two,c.winner from ind_cup_confrontation c join ind_cup_round r on r.id = c.id and r.nRound = c.nRound where r.id = 1;
        db.query("select c.id,c.nRound,c.player_one,c.player_two,c.winner from ind_cup_confrontation c join ind_cup_round r on r.id = c.id and r.nRound = c.nRound where r.id = ? order by c.nRound",
        [id],(err,resQ)=>{

            if(err!=null)
                rej(err);

            
            var json = [];
            var confrontations = [];


            for (let index = 0; index < resQ.length; index++) {
                const line = resQ[index];

                //Obtenemos los datos de la confrontación de cada fila y lo añadimos al array de confrontations
                var confrontation = {
                    player_one: line.player_one,
                    player_two: line.player_two,
                    winner: line.winner,
                };

                confrontations.push(confrontation);

                //Comprobamos si el número de ronda actual es la última vez que aparece o si es la última linea directamente
                //En este caso añadimos la ronda con las confrontacioens recogidas hasta ahora


                if(index == resQ.length-1 || line.nRound != resQ[index+1].nRound){
                    var round = {
                        nRound: line.nRound,
                        confrontations: confrontations
                    }

                    //Añadimos la ronda al array respuesta
                    json.push(round);

                    //Limpiamos las confrontaciones de esta ronda que ya quedo registrada
                    confrontations = [];
                }
            }

            done(json);
        });
    });
}



//JOINED
function getIndividualCupsJoined (id){
    //select id,name,(select name from user u where u.id = c.admin) as admin, state,expire,(select title from videogame v where v.id = c.videogame) as game, type,(select round_name from ind_cup_round where id = c.id AND nRound = (SELECT MAX(nRound) from ind_cup_round where id = c.id)) as actualRound  from competition c where c.id in (select cup from ind_cup_participant where user =1) AND expired = 0;
    return new Promise(function(done,rej){
        db.query(" select id,name,(select name from user u where u.id = c.admin) as admin, state,expire,(select title from videogame v where v.id = c.videogame) as game, type, "+
        "(SELECT MAX(nRound) from ind_cup_round where id = c.id) as actualRound, (select nRounds from ind_cup where id = c.id) as totalRounds, chat, description from competition c where c.id in "+
        "(select cup from ind_cup_participant where user =?) AND expired = 0;",[id],(error,result)=>{
            if(error!=null){
                rej(error);
            }else
                done(result);
        });
    });
};

function getIndividualLeaguesJoined (id){
    //select id,name,(select name from user u where u.id = c.admin) as admin, state,expire,(select title from videogame v where v.id = c.videogame) as game, type,(select MAX(league_day) from ind_league_day where id = c.id) as actualRound  from competition c where c.id in (select league from ind_league_participant where user =1) AND expired = 0;
    return new Promise(function(done,rej){
        db.query("select id,name,(select name from user u where u.id = c.admin) as admin, state,expire,(select title from videogame v where v.id = c.videogame) as game, type,(select MAX(league_day) "+
        "from ind_league_day where id = c.id) as actualRound, (select nLeague_days from ind_league where id = c.id) as totalRounds, chat, description from competition c where c.id in (select league from ind_league_participant where user =?) AND expired = 0;",[id],(error,result)=>{
            if(error!=null){
                console.log(error);
                rej(error);
            }else
                done(result);
        });
    });
}

function getTeamCupsJoined (id){
    // select id,name,(select name from user u where u.id = c.admin) as admin, state,expire,(select title from videogame v where v.id = c.videogame) as game, type  from competition c where c.id in (select cup from team_cup_participant where team = (select team from user where id = 3)) AND expired = 0;
    return new Promise(function(done,rej){
        db.query("select id,name,(select name from user u where u.id = c.admin) as admin, state,expire, " + 
        "(select title from videogame v where v.id = c.videogame) as game, type, (SELECT MAX(nRound) from team_cup_round where id = c.id) as actualRound, (select nRounds from team_cup where id = c.id) as totalRounds, chat, description " +
        "from competition c where c.id in (select cup from team_cup_participant where team = (select team from user where id = ?)) "+ 
        "AND expired = 0;",[id],(error,result)=>{
            if(error!=null){
                console.log(error);
                rej(error);
            }else
                done(result);
        });
    });
}

function getTeamLeaguesJoined (id){
    return new Promise(function(done,rej){
        db.query("select id,name,(select name from user u where u.id = c.admin) as admin, state,expire, " + 
        "(select title from videogame v where v.id = c.videogame) as game, type, (select MAX(league_day) from team_league_day where id = c.id) as actualRound, (select nLeague_days from team_league where id = c.id) as totalRounds,chat, description from competition c " + 
        "where c.id in (select league from team_league_participant where team = (select team from user where id = ?)) "+ 
        " AND expired = 0;",[id],(error,result)=>{
            if(error!=null){
                console.log(error);
                rej(error);
            }else
                done(result);
        });
    });
}

module.exports = router;