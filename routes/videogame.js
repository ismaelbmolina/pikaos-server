var express = require('express');
var router = express.Router();
var db = require('../db');
var errors = require("../errors");
var email = require("../email");

//Petición para que los usuarios propongan videojuegos.
//Entrada videogame y description
//200: Todo ok
router.post('/proposal',(req,res)=>{
    try{
        var videogame = req.body.name;
        var information = req.body.information;
        var individual = req.body.individual;
        var team = req.body.team;
        var customGames = req.body.customGames;

        email.videogameProposal(videogame,information,individual,team,customGames);
        res.status(200).send("Propuesta mandada con éxito");
    }catch(e){
        res.status(500).send();
    }
});

router.get('/',(req,res)=>{
    db.query("select id,title from videogame order by title",(error,videogames)=>{
        res.status(200).send(videogames);
    });
});

router.get('/individual',(req,res)=>{
    db.query("select id,title from videogame where individual = 1 order by title",(error,videogames)=>{
        res.status(200).send(videogames);
    });
});

router.get('/team',(req,res)=>{
    db.query("select id,title from videogame where teams = 1 order by title",(error,videogames)=>{
        res.status(200).send(videogames);
    });
});

module.exports = router;